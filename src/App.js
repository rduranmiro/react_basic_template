import React from "react";
import { hot } from "react-hot-loader";
import "./App.css";
import HelloWorld from './components/HelloWorld'

const App = () => {
  return (
    <div className="App">
      <HelloWorld />
    </div>
  );
};

export default hot(module) (App);
