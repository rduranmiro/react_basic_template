import React from "react";
import logo from "../../public/logo.png";

const HelloWorld = () => {
  return (
    <div className="HelloWorld">
      <h1>Hello World !!</h1>
      <img src={logo} alt="logo" />
    </div>
  );
};

export default HelloWorld;
